CREATE DATABASE IF NOT EXISTS Nova_Poshta
CHARACTER SET utf8
COLLATE utf8_general_ci;

USE Nova_Poshta;

SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS region;
CREATE TABLE region (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
region_name VARCHAR(45) NOT NULL
);

DROP TABLE IF EXISTS city;
CREATE TABLE city (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
city_name VARCHAR(45) NOT NULL,
region_id INT NOT NULL
);

DROP TABLE IF EXISTS working_hours;
CREATE TABLE working_hours (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
monday_friday_open_hour VARCHAR(5) NOT NULL DEFAULT '08:00',
monday_friday_close_hour VARCHAR(5) NOT NULL DEFAULT '20:00',
saturday_open_hour VARCHAR(5) NOT NULL DEFAULT '09:00',
saturday_close_hour VARCHAR(5) NOT NULL DEFAULT '19:00',
sunday_open_hour VARCHAR(5) NOT NULL DEFAULT '10:00',
sunday_close_hour VARCHAR(5) NOT NULL DEFAULT '16:00'
);

DROP TABLE IF EXISTS position;
CREATE TABLE position (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
position_name VARCHAR(45) NOT NULL DEFAULT 'Operator',
salary DECIMAL(8,2) NOT NULL DEFAULT 4173.00
);

DROP TABLE IF EXISTS bonus_card;
CREATE TABLE bonus_card (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
bonus DECIMAL(8,2) NOT NULL DEFAULT 0.00
);

DROP TABLE IF EXISTS department;
CREATE TABLE department (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
number INT NOT NULL,
street VARCHAR(45) NOT NULL,
additional_info VARCHAR(45),
weight_limit INT NOT NULL DEFAULT 0,
working_hours_id INT NOT NULL,
city_id INT NOT NULL
);

DROP TABLE IF EXISTS employee;
CREATE TABLE employee (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
second_name VARCHAR(45) NOT NULL,
first_name VARCHAR(45) NOT NULL,
middle_name VARCHAR(45),
additional_info VARCHAR(45),
weight_limit INT NOT NULL DEFAULT 0,
department_id INT NOT NULL,
position_id INT NOT NULL
);

DROP TABLE IF EXISTS client;
CREATE TABLE client (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
second_name VARCHAR(45) NOT NULL,
first_name VARCHAR(45) NOT NULL,
middle_name VARCHAR(45),
phone VARCHAR(25),
default_department_id INT NOT NULL,
bonus_card_id INT NOT NULL
);

DROP TABLE IF EXISTS parcel;
CREATE TABLE parcel (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
weight DECIMAL(9,1) NOT NULL DEFAULT 0.5,
price DECIMAL(8,2) NOT NULL DEFAULT 35.00,
is_paid VARCHAR(3) NOT NULL DEFAULT 'no',
from_department_id INT NOT NULL,
to_department_id INT NOT NULL,
sender_client_id INT NOT NULL,
receiver_client_id INT NOT NULL
);

DROP TABLE IF EXISTS transport;
CREATE TABLE transport (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
car_number VARCHAR(45) NOT NULL
);

DROP TABLE IF EXISTS driver;
CREATE TABLE driver (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
second_name VARCHAR(45) NOT NULL,
first_name VARCHAR(45) NOT NULL,
middle_name VARCHAR(45),
phone VARCHAR(25)
);

DROP TABLE IF EXISTS driver_license;
CREATE TABLE driver_license (
driver_id INT NOT NULL,
transport_id INT NOT NULL
);

SET FOREIGN_KEY_CHECKS=1;

ALTER TABLE city
ADD FOREIGN KEY (region_id)
REFERENCES region (id)
ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE department
ADD FOREIGN KEY (working_hours_id)
REFERENCES working_hours (id)
ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE department
ADD FOREIGN KEY (city_id)
REFERENCES city (id)
ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE employee
ADD FOREIGN KEY (position_id)
REFERENCES position (id)
ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE employee
ADD FOREIGN KEY (department_id)
REFERENCES department (id)
ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE client
ADD FOREIGN KEY (bonus_card_id)
REFERENCES bonus_card (id)
ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE client
ADD FOREIGN KEY (default_department_id)
REFERENCES department (id)
ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE parcel
ADD FOREIGN KEY (from_department_id)
REFERENCES department (id)
ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE parcel
ADD FOREIGN KEY (to_department_id)
REFERENCES department (id)
ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE parcel
ADD FOREIGN KEY (sender_client_id)
REFERENCES client (id)
ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE parcel
ADD FOREIGN KEY (receiver_client_id)
REFERENCES client (id)
ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE driver_license
ADD FOREIGN KEY (driver_id)
REFERENCES driver (id)
ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE driver_license
ADD FOREIGN KEY (transport_id)
REFERENCES transport (id)
ON DELETE NO ACTION ON UPDATE CASCADE;

INSERT INTO region (region_name)
VALUES  ('Avtonomna Respublika Krym'),
		('Vinnytska'),
		('Volynska'),
		('Dniprovska'),
		('Donetska'),
		('Zhytomyrskа'),
		('Zakarpatska'),
		('Zaporizka'),
        ('Ivano-Frankivska'),
        ('Kyivska'),
        ('Kropyvnytska'),
        ('Luhanska'),
        ('Lvivska'),
        ('Mykolaivska'),
        ('Odeska'),
        ('Poltavska'),
        ('Rivnenska'),
        ('Sumska'),
        ('Ternopilska'),
        ('Kharkivska'),
        ('Khersonska'),
        ('Khmelnytska'),
        ('Cherkaska'),
        ('Chernivetska'),
        ('Chernihivska');
        
INSERT INTO city (region_id, city_name)
VALUES  (13, 'Brody'),
		(13, 'Busk'),
        (13, 'Horodok'),
        (13, 'Drohobytch'),
        (13, 'Zhydachiv'),
        (13, 'Zhovkva'),
        (13, 'Zolochiv'),
        (13, 'Mykolaiv'),
        (13, 'Mostyska'),
        (13, 'Peremyshliany'),
        (13, 'Pustomyty'),
        (13, 'Radehiv'),
        (13, 'Sambir'),
        (13, 'Skole'),
        (13, 'Sokal'),
        (13, 'Staryi Sambir'),
        (13, 'Turka'),
        (13, 'Javoriv'),
        (10, 'Baryshivka'),
        (10, 'Bila Cerkva'),
        (10, 'Bohuslav'),
        (10, 'Boryspil'),
        (10, 'Borodianka'),
        (10, 'Brovary'),
        (10, 'Vasylkiv'),
        (10, 'Vyshhorod'),
        (10, 'Zhurivka'),
        (10, 'Kaharlyk'),
        (10, 'Kyiv'),
        (10, 'Makariv'),
        (10, 'Myronivka'),
        (10, 'Pereiaslav-Khmelnytskyi'),
        (10, 'Rokytne'),
        (10, 'Stavyshche'),
        (10, 'Tarashcha'),
        (10, 'Tetijiv'),
        (10, 'Jahotyn');
        
INSERT INTO working_hours (	monday_friday_open_hour, monday_friday_close_hour, saturday_open_hour, saturday_close_hour, sunday_open_hour, sunday_close_hour)
VALUES  (DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT);
        
INSERT INTO department (city_id, number, working_hours_id, street, additional_info, weight_limit)
VALUES  (13, 1, 1, 'вул. Пирогівський шлях, 135', '', 0),
		(13, 2, 1, 'вул. Бережанська, 9', '(Оболонь Лугова)', 0),
        (13, 3, 1, 'вул. Калачівська, 13', '(Стара Дарниця)', 0),
        (13, 4, 1, 'вул. Кричевського, 19, м. Житомирська', '(Святошин)', 0),
        (13, 5, 1, 'вул. Федорова, 32', '(м. Олімпійська)', 0),
        (13, 6, 1, 'вул. Миколи Василенка, 2', '(метро Берестейська)', 0),
        (13, 7, 1, 'вул. Гната Хоткевича, 8', '(м.Чернігівська)', 5),
        (13, 8, 1, 'вул. Набережно-Хрещатицька, 33', '', 30),
        (13, 9, 1, 'пров. В\'ячеслава Чорновола, 54а', '(р-н Жулянського мосту)', 0),
        (13, 10, 1, 'вул. Василя Жуковського, 22А', '', 0),
        (13, 11, 1, 'просп. Лобановського, 119', '(ран.Червонозоряний) (м.Деміїв.)', 30),
        
        (10, 1, 1, 'вул. Городоцька, 355/6', '', 0),
        (10, 2, 1, 'вул. Пластова, 7', '', 0),
        (10, 3, 1, 'вул. Угорська, 22', '(заїзд з вул Луганської)', 0),
        (10, 4, 1, 'вул. Кульпарківська, 93а', '', 0),
        (10, 5, 1, 'вул. Данилишина, 6', '', 30),
        (10, 6, 1, 'вул. Сихівська, 8', '', 30),
        (10, 7, 1, 'вул. Личаківська, 8', '', 30),
        (10, 8, 1, 'вул. Грінченка, 2а', '', 30),
        (10, 9, 1, 'вул. Кульпарківська, 142', '', 30),
        (10, 10, 1, 'вул. Левицького, 7', '', 30),
        (10, 11, 1, 'вул. Городоцька, 120', '', 30),
        (10, 12, 1, 'вул. Сяйво, 13', '(р-н Левандівка)', 30),
        (10, 13, 1, 'вул. Ставова, 7в', '', 30);
        
INSERT INTO position (position_name, salary)
VALUES  ('Operator', 5000),
		('Loader', DEFAULT),
		('Manager', 9000),
		('Сhief', 25000),
		('Driver', 13000),
		('Trainee', DEFAULT);
        
INSERT INTO employee (second_name, first_name, middle_name, department_id, position_id)
VALUES  ('Ivanov', 'Ivan', 'Ivanovych', 1, 2),
		('Petrov', 'Petro', 'Petrovych', 1, 1),
        ('Ivanova', 'Olena', 'Gavrylivna', 1, 3),
        ('Ivanov', 'Ivan', 'Fedorovych', 1, 4),
		('Yushchenko', 'Igor', 'Fedorovych', 1, 5),
		('Malyi', 'Vova', '', 1, 6),
        
        ('Igorov', 'Igor', 'Igorovych', 13, 2),
		('Pavlov', 'Pavlo', 'Pavlovych', 13, 1),
        ('Babij', 'Olena', 'Ivanivna', 13, 3),
        ('Babij', 'Fedir', 'Grygorovych', 13, 4),
		('Kuchma', 'Iosyf', 'Petrovych', 13, 5),
		('Melkyi', 'Vasia', '', 13, 6);
        
INSERT INTO bonus_card (bonus)
VALUES  (111),
		(38);
        
INSERT INTO client (second_name, first_name, middle_name, phone, default_department_id, bonus_card_id)
VALUES  ('Drohobytskyy', 'Roman', 'Ivanovych', '+38 066 666 66 66', 12, 1),
		('Bobalo', 'Oleg', 'Fedorovych', '+38 063 333 44 55', 1, 2);
        
INSERT INTO parcel (weight, price, is_paid, from_department_id, to_department_id, sender_client_id, receiver_client_id)
VALUES  (2, 50, 'yes', 1, 12, 2, 1),
		(7.5, 110, DEFAULT, 12, 1, 1, 2);
        
INSERT INTO driver (second_name, first_name, middle_name, phone)
VALUES  ('Vodiatl', 'Sergij', 'Sergijovych', '+38 050 050 50 50'),
		('Dalnoboj', 'Oleg', 'Valerijovych', '+38 097 000 00 97');
        
INSERT INTO transport (car_number)
VALUES  ('AA 3348 EE'),
		('AA 4585 HP'),
		('AA 1111 BP');
        
INSERT INTO driver_license (driver_id, transport_id)
VALUES  (1, 3),
		(2, 1),
		(2, 2),
		(2, 3);        
        
        